var bloodDonarsController = require('../controllers/blood_donars_controller');

module.exports = function(router) {
    console.log('Blood Donar Management APIs');
    router
        .route('/api/bloodDonars/create')
        .post(bloodDonarsController.createDonar);
     router
        .route('/api/bloodDonars/update/:id')
        .put(bloodDonarsController.updateDonar);
    router
        .route('/api/bloodDonars/delete/:id')
        .delete(bloodDonarsController.deleteDonar);
    router
        .route('/api/bloodDonars/viewAllDonars')
        .get(bloodDonarsController.donarsList);
    router
        .route('/api/bloodDonars/viewDonarByID/:id')
        .get(bloodDonarsController.donarByID);
    return router;
}


//GET -  1024 - passed through URL
//POST - form submitting - submitting via Object - payload
//PUT  - form submitting - submitting via Object
//DELETE - Submitting data for deletion
//OPTIONS