const { json } = require('express');
const bloodDonarsRepo = require('../../repositories/blood_donars_repo');

module.exports.createDonar = function(request, response) {
    response.setHeader('content-Type', 'application/json');
    bloodDonarsRepo.createDonar(request.body, function (error, result) {
        if(error) {
            response.status(400).json(error);        
        } 
        if(result) {
            response.json(result);        
        } 
    });
}

module.exports.updateDonar = function(request, response) {
    response.setHeader('content-Type', 'application/json');
    bloodDonarsRepo.updateDonar(request.params.id, request.body, function (error, result) {
        if(error) {
            response.status(400).json(error);        
        } 
        if(result) {
            response.status(200).json({"message" : result});        
        } 
    });
}

module.exports.donarsList = function(request, response) {
    response.setHeader('content-Type', 'application/json');
    bloodDonarsRepo.donarsList(function (error, result) {
        if(error) {
            response.status(400).json(error);        
        } 
        if(result) {
            const detail_rows = result.rows ? result.rows : [];
            response.json(detail_rows);        
        } 
    });
}

module.exports.donarByID = function(request, response) {
    response.setHeader('content-Type', 'application/json');
    bloodDonarsRepo.donarByID(request.params.id, function (error, result) {
        if(error) {
            response.status(400).json(error);        
        } 
        if(result) {
            const detail_rows = result.rows ? result.rows : [];
            response.json(detail_rows);        
        } 
    });
}

module.exports.deleteDonar = function(request, response) {
    response.setHeader('content-Type', 'application/json');
    bloodDonarsRepo.deleteDonar(request.params.id, function (error, result) {
        if(error) {
            response.status(400).json(error);        
        } 
        if(result) {
            response.status(200).json({"mesage" : result}); 
        } 
    });
}