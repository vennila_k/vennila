const psql_client = require('../db/postgres');


module.exports.createDonar = (donor, callback) => {
  psql_client.insert(donor).into("t_blooddonars").returning("*")
  .then(rows => callback(null, rows))
  .catch(error => callback(error, null));
}

module.exports.updateDonar = (id, donor, callback) => {
  psql_client.from('t_blooddonars').where('id', '=', id).update(donor)
  .then(() => callback(null, `Donar id = ${id} updated successfully`))
  .catch(error => callback(error, null));
}

module.exports.donarsList = (callback) => {
  const query = "select * from t_blooddonars";
  psql_client.raw(query)
  .then(rows => callback(null, rows))
  .catch(error => callback(error, null));
}

module.exports.donarByID = (id, callback) => {
  const query = `select * from t_blooddonars where id=${id}`;
  psql_client.raw(query)
  .then(rows => callback(null, rows))
  .catch(error => callback(error, null));
}

module.exports.deleteDonar = (id, callback) => {
  psql_client.from('t_blooddonars').where({id}).del()
  .then(() => callback(null, `Donar id = ${id} was deleted successfully`))
  .catch(error => callback(error, null));
}